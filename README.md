# README #

Examples for using a properties file for using sensitive information (e.g. API keys, passwords), without storing that information in a repository.

Simply copy over `.properties_sample_R` or `.properties_sample_py` (depending on your codebase) to `.properties` (which is ignored by git). Then, add your settings, and run the corresponding code (`use_properties.R` or `user_properties.py`, respectively).